"use client"
import React, { useState } from 'react';

const CardMenu = ({ menuData, styles }) => {
  const [activeMenu, setActiveMenu] = useState(3);
  
  const handleMenuClick = (id) => {
    setActiveMenu(id);
  };

  return (
    <div className='card g-bx-shadow-lg g-bdr-round border-0'>
      <div className='p-3'>
        <div className='row'>
          {menuData.map((row, index) => (
            <div
              className="d-flex align-items-center p-2 px-3 mb-2"
              key={index}
              onClick={() => handleMenuClick(row.id)}
            >
              <i className={activeMenu === row.id ? `${styles['card_menu--icon_active']} ${row.icon} me-2` : `${styles['card_menu--icon']} ${row.icon} me-2` } />
              <span className={activeMenu === row.id ? `${styles['card_menu--title_active']}` : `${styles['card_menu--title']}`}>
                {row.name}
              </span>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}

export default CardMenu
