import React from 'react'

const CardProgress = ({ styles }) => {
  const value = 50
  
  return (
    <div className={`${styles['card_progress--section']} card g-bx-shadow-lg g-bdr-round`}>
      <div className='p-3'>
        <p className={`${styles['card_progress--title']}`}>Profile Completion Progress</p>
        <div className='d-flex'>
          <input
            type="range"
            value={value}
            max="100"
            className={`${styles['card_progress--range']} mb-3 mb-md-0 me-md-3`}
            readOnly
          />
          <span className={`${styles['card_progress--value']} ms-2`}>{value}%</span>
        </div>
      </div>
    </div>
  )
}

export default CardProgress
