"use client"
import React, { useState } from 'react'

const CardAsessment = ({ styles }) => {
  const [tab, setTab] = useState(1)

  const handleTabClick = (tab) => {
    setTab(tab)
  }

  return (
    <div className='card g-bx-shadow-lg g-bdr-round border-0'>
      <div className='p-3'>
        <div className='row'>
          <div className='col-md-6 text-center'>
            <span
              className={tab === 1 ? `${styles['card_assesment--title_active']}` : `${styles['card_assesment--title']}`}
              onClick={() => handleTabClick(1)}
            >
              Technical Skills
            </span>
          </div>
          <div className='col-md-6 text-center'>
            <span
              className={tab === 2 ? `${styles['card_assesment--title_active']}` : `${styles['card_assesment--title']}`}
              onClick={() => handleTabClick(2)}
            >
              Personality
            </span>
          </div>
        </div>
        <hr />
        <div className='d-flex mt-2'>
          {tab === 1 &&
            <div className='container-fluid'>
              <div className='d-flex justify-content-between align-items-center'>
                <span className={`${styles['card_assesment--title']}`}>Technical Skills</span>
                <button className={`${styles['card_assesment--button']}`}>Edit</button>
              </div>
              <div className='d-flex justify-content-between align-items-center mt-3'>
                <div className='d-flex align-items-center'>
                  <i className={`${styles['card_assesment--switch']} bx bx-toggle-right`}></i>
                  <div className='ms-2'>
                    <div>
                      <span className={`${styles['card_assesment--value_title']}`}>Javascript</span>
                      <button className={`${styles['card_assesment--value1']} ms-2`}>Basic</button>
                    </div>
                    <span className={`${styles['card_assesment--value_subtitle']} d-block mt-2`}>Last Taken Jul 2, 2023</span>
                    <span className={`${styles['card_assesment--value_viewtitle']} d-block mt-2`}>View Assesment History</span>
                  </div>
                </div>
                <div>
                  <div className={`${styles['card_assesment--score']} card`}>
                    <div className='text-center'>
                      <span className={`${styles['card_assesment--scoretitle']} me-1`}>Score</span>
                      <span className={`${styles['card_assesment--persen']}`}>80%</span>
                    </div>
                    <button className={`${styles['card_assesment--buttonscore']} mt-2`}>Good</button>
                  </div>
                </div>
              </div>
              <hr />
              <div className='d-flex justify-content-between align-items-center mt-3'>
                <div className='d-flex align-items-center'>
                  <i className={`${styles['card_assesment--switch']} bx bx-toggle-right`}></i>
                  <div className='ms-2'>
                    <div>
                      <span className={`${styles['card_assesment--value_title']}`}>Java </span>
                      <button className={`${styles['card_assesment--value1']} ms-2`}>Basic</button>
                    </div>
                    <span className={`${styles['card_assesment--value_subtitle']} d-block mt-2`}>Last Taken Jul 2, 2023</span>
                    <span className={`${styles['card_assesment--value_viewtitle']} d-block mt-2`}>View Assesment History</span>
                  </div>
                </div>
                <div>
                  <div className={`${styles['card_assesment--score']} card`}>
                    <div className='text-center'>
                      <span className={`${styles['card_assesment--scoretitle']} me-1`}>Score</span>
                      <span className={`${styles['card_assesment--persen']}`}>50%</span>
                    </div>
                    <button className={`${styles['card_assesment--buttonscorefail']} mt-2`}>Poor</button>
                  </div>
                </div>
              </div>
              <div className='d-flex justify-content-end align-items-center mt-3'>
                <i className={`${styles['card_assesment--icon_remove']} bx bxs-trash-alt me-2`}></i>
                <span className={`${styles['card_assesment--text_remove']}`}>Remove</span>
              </div>
            </div>
          }
          {tab === 2 &&
            <div className='container-fluid'>
              <div className='d-flex justify-content-between align-items-center'>
                <span className={`${styles['card_assesment--title']}`}>
                  Personality
                </span>
                <button className={`${styles['card_assesment--button']}`}>Take Another Test</button>
              </div>
              <div className='d-flex justify-content-between align-items-center mt-3'>
                <div className='d-flex align-items-center'>
                  <div>
                    <div className='d-flex align-items-center'>
                      <span className={`${styles['card_assesment--value_title']}`}>DISC</span>
                      <i className={`${styles['card_assesment--icon_info']} bx bxs-info-circle ms-2`}></i>
                    </div>
                    <span className={`${styles['card_assesment--value_subtitle']} d-block mt-2`}>Last Taken Jul 2, 2023</span>
                    <span className={`${styles['card_assesment--value_viewtitle']} d-block mt-2`}>View Test History</span>
                  </div>
                </div>
                <div>
                  <div>
                    <button className={`${styles['card_assesment--button']}`}>View Result</button>
                  </div>
                </div>
              </div>
            </div>
          }
        </div>
      </div>
    </div>
  )
}

export default CardAsessment
