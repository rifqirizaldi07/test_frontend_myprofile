import Image from 'next/image';
import React from 'react'
import Profile from "../../assets/profile.jpeg"

const CardProfile = ({ userData, styles }) => {
  const dataProfile = userData.profile
  return (
    <div className='card g-bx-shadow-lg g-bdr-round border-0 p-3 mb-3'>
      <div className='d-flex justify-content-between'>
        <div className='d-flex align-items-center'>
          <div>
            <Image
              src={Profile}
              width={65}
              height={65}
              alt='Profile'
              placeholder="blur"
              className={`${styles['card_profile--avatar']}`}
            />
          </div>
          <div className='ms-3'>
            <div className='mb-1'>
              <span className={`${styles['card_profile--name']}`}>{dataProfile.name}</span>
              <i className='bx bxs-flag-alt ms-1'></i>
            </div>
            <span className={`${styles['card_profile--role']}`}>{dataProfile.role}</span>
          </div>
        </div>
        <div className='d-flex'>
          <div>
            <i className={`${styles['card_profile--icon']} bx bxs-edit-alt me-2`}></i>
            <span className={`${styles['card_profile--edit']}`}>Edit</span>
          </div>
        </div>
      </div>
      <div className='row mt-3'>
        <div className="col-lg">
          <span className={`${styles['card_profile--title']}`}>Phone Number</span>
          <span className={`${styles['card_profile--subtitle']} d-block`}>(+62) {dataProfile.phone}</span>
        </div>
        <div className="col-lg">
          <span className={`${styles['card_profile--title']}`}>Email</span>
          <span className={`${styles['card_profile--subtitle']} d-block`}> {dataProfile.email}</span>
        </div>
        <div className="col-lg">
          <span className={`${styles['card_profile--title']}`}>Gender</span>
          <span className={`${styles['card_profile--subtitle']} d-block`}> {dataProfile.gender}</span>
        </div>
        <div className="col-lg">
          <span className={`${styles['card_profile--title']}`}>Religion</span>
          <span className={`${styles['card_profile--subtitle']} d-block`}> {dataProfile.religion}</span>
        </div>
        <div className="col-lg">
          <span className={`${styles['card_profile--title']}`}>Place and Date of Birth</span>
          <span className={`${styles['card_profile--subtitle']} d-block`}> {dataProfile.birthday}</span>
        </div>
      </div>
      <div className='mt-3'>
        <div className="col-lg">
          <span className={`${styles['card_profile--title']}`}>Address</span>
          <span className={`${styles['card_profile--subtitle']} d-block`}>{dataProfile.address}</span>
        </div>
      </div>
      <div className='mt-3'>
        <div className="col-lg">
          <span className={`${styles['card_profile--title']}`}>About Me</span>
          <span className={`${styles['card_profile--subtitle']} d-block`}>{dataProfile.about}</span>
        </div>
      </div>
    </div>
  )
}

export default CardProfile


