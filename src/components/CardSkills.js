import React from 'react'

const CardSkills = ({ styles }) => {
  return (
    <div className='card g-bx-shadow-lg g-bdr-round border-0'>
      <div className='p-3'>
        <p className={`${styles['card_skill--title']}`}>Skills</p>
        <div className='d-block'>
          <button className={`${styles['card_skill--value1']} me-2 mb-2`}>Javascript</button>
          <button className={`${styles['card_skill--value2']} me-2 mb-2`}>React</button>
          <button className={`${styles['card_skill--value2']} me-2 mb-2`}>Next JS</button>
          <button className={`${styles['card_skill--value2']} me-2 mb-2`}>Redux</button>
          <button className={`${styles['card_skill--value3']} me-2 mb-2`}>Frontend Development</button>
        </div>
        <span className={`${styles['card_skill--more']} mt-2`}>and 6 More.</span>
        <div className='mt-3 text-center'>
          <button className={`${styles['card_skill--button']} col-md-12 g-bdr-round`}>View All</button>
        </div>
      </div>
    </div>
  )
}

export default CardSkills
