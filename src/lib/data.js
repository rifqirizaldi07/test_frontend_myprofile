export const userData = {
    user_id: 1,
    username: "rifqi",
    profile : {
      name : "Rifqi Rizaldi Putra",
      role: "Frontend Developer",
      phone: "082177198865",
      email: "rifqirizaldiputra@gmail.com",
      gender: "Male",
      religion: "Islam",
      birthday: "Padang, 07 September 1996",
      address: "Jalan Ciujung No 40, Cideng Barat Kecamatan Gambir, Jakarta Pusat",
      about: "Hello my name is Rifqi I am a Frontend Developer, my experience in the field has been 3 years, the programming language I mastered is Javascript and Framework mastered namely React JS, Next JS and for mobile development I mastered React Native, now I am starting to learn about Typescript and basic concepts of OOP. I am a Frontend Developer, my experience in the field has been 3 years, the programming language I mastered is Javascript and Framework mastered namely React JS, Next JS and for mobile development I mastered React Native,"
    }
} 

export const menuData = [
  {
    id: 1,
    name: "Education",
    icon: "bx bxs-layer"
  },
  {
    id: 2,
    name: "Work Experience",
    icon: "bx bxs-briefcase"
  },
  {
    id: 3,
    name: "Assesments",
    icon: "bx bxs-file-blank"
  },
  {
    id: 4,
    name: "Resume and Portofolio",
    icon: "bx bxs-file"
  },
  {
    id: 5,
    name: "Achievements & Certification",
    icon: "bx bxs-certification"
  },
  {
    id: 6,
    name: "Languange",
    icon: "bx bxs-user-detail"
  },
  {
    id: 7,
    name: "Job Preferences",
    icon: "bx bxs-cog"
  },
  {
    id: 8,
    name: "Additional Information",
    icon: "bx bxs-book-open"
  },
]