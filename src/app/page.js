import React from "react"
import { userData, menuData } from "@/lib/data"
import CardProfile from "@/components/CardProfile"
import CardProgress from "@/components/CardProgress"
import CardSkills from "@/components/CardSkills"
import CardMenu from "@/components/CardMenu"
import CardAsessment from "@/components/CardAsessment"
import styles from "./page.module.css"

export default function Home() {
  return (
    <div className='container-fluid'>
      <div className="p-3 mt-2">
        <div className='d-flex justify-content-between align-items-center'>
          <h4 className={`${styles['profile_section--titile']}`}>Profile</h4>
          <div>
            <button className={`${styles['profile_section--button']} g-bdr-round`}>
              Generate CV
            </button>
          </div>
        </div>
        <div className="d-flex row mt-4">
          <div className="col-md-9">
            <CardProfile userData={userData} styles={styles} />
          </div>
          <div className="col-md-3">
            <div className="mb-3">
              <CardProgress styles={styles} />
            </div>
            <div className="mb-3">
              <CardSkills styles={styles} />
            </div>
          </div>
        </div>
        <div className="d-flex row">
          <div className="col-md-4 mb-3">
            <CardMenu menuData={menuData} styles={styles} />
          </div>
          <div className="col-md-8">
            <CardAsessment styles={styles} />
          </div>
        </div>
      </div>
    </div>
  )
}
