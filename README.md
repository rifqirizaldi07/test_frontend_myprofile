# My Profile

## Getting Started

### Clone Project

#### with SSH

`git@gitlab.com:rifqirizaldi07/test_frontend_myprofile.git`

#### with HTTPS

`https://gitlab.com/rifqirizaldi07/test_frontend_myprofile.git`

### Installation

run `npm install` or `yarn install`

### Running Project

For development:

`npm run dev`

## Technology Stack

1. Next JS Version : 13.5.6
2. React JS Version : 18.0
3. Boostraps Version : 5.3.0
4. BoxIcons Version : 2.1.4

### Node Version
`18.14.0`

### Screenshot Project

![My Profile Screenshot](/assets/myprofile.png)